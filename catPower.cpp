///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Dane Sears dsears@hawaii.edu
/// @date   31_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const double JOULES_IN_A_MEGATON = 4.184e15;
const double JOULES_IN_A_GGE = 121300000;
const double JOULES_IN_A_FOE = 1e44;

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';
const char MEGATON = 'm';
const char GGE = 'g';
const char FOE = 'f';
const char CATPOWER = 'c';

double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}

double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}

double fromMegatonToJoule( double megaton ) {
   return megaton * JOULES_IN_A_MEGATON;
}

double fromJouleToMegaton( double joule ) {
   return joule / JOULES_IN_A_MEGATON;
}

double fromGGEToJoule( double gge) {
   return gge * JOULES_IN_A_GGE;
}

double fromJouleToGGE( double joule) {
   return joule / JOULES_IN_A_GGE;
}

double fromFOEToJoule( double foe) {
   return foe * JOULES_IN_A_FOE;
}

double fromJouleToFOE( double joule) {
   return joule / JOULES_IN_A_FOE;
}

double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}

double fromJouleToCatPower( double catPower ) {
   return 0.0;
}




int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

   //printf( "fromValue = [%lG]\n", fromValue );   
   //printf( "fromUnit = [%c]\n", fromUnit );     
   //printf( "toUnit = [%c]\n", toUnit );        

   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
      case MEGATON       : commonValue = fromMegatonToJoule( fromValue );
                           break;
      case GGE           : commonValue = fromGGEToJoule( fromValue );
                           break;
      case FOE           : commonValue = fromFOEToJoule( fromValue );
                           break;
      case CATPOWER      : commonValue = fromCatPowerToJoule( fromValue );
   }

   //printf( "commonValue = [%lG] joule\n", commonValue ); 


	 
	
double toValue;
   switch ( toUnit ){
      case JOULE         : toValue = commonValue;
                           break;
      case ELECTRON_VOLT : toValue = fromJouleToElectronVolts( commonValue );
                           break;
      case MEGATON       : toValue = fromJouleToMegaton( commonValue );
                           break;
      case GGE           : toValue = fromJouleToGGE( commonValue );
                           break;
      case CATPOWER      : toValue = fromJouleToCatPower( commonValue );
                           break;
   }

   
   
   //printf( "commonValue = [%lG] joule\n", commonValue ); 


   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}
